<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h2> Register Event Form</h2>
<form action="registerParticipant" method="post">
	<table>
		<tr><td>Participant Id</td> <td><input type="number" name="participantId"/></td></tr>
		<tr><td>Participant Name</td> <td><input type="text" name="participantName"/></td></tr>
		<tr><td>Mobile no</td> <td><input type="text" name="mobileNo"/></td></tr>
		<tr><td>Address</td> <td><input type="text" name="address"/></td></tr>
		<tr><td>Event Id</td> <td><input type="number" name="eventId"/></td></tr>
		<tr><td><input type="submit" value="Register participant"/></td></tr>
	</table>
</form>
</body>
</html>