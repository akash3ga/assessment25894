<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
   <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<body>
	<h2>Employees Details</h2>
	<table>
		<thead>
			<tr>
				<th>Participant Id</th>
				<th>Participant Name</th>
				<th>Mobile NO</th>
				<th>Address</th>
				<th>Event Id</th>
				<th>Event Name</th>
				<th>location</th>
				<th>capacity</th>
				<th>Event status</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="participant" items="${participants}">
				<tr>
					<td>${participant.participantId}</td>
					<td>${participant.participantName}</td>
					<td>${participant.mobileNo}</td>
					<td>${participant.address}</td>
					<td>${participant.event.eventId}</td>
					<td>${participant.event.eventName}</td>
					<td>${participant.event.location}</td>
					<td>${participant.event.capacity}</td>
					<td>${participant.event.eventStatus}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</body>
</body>
</html>