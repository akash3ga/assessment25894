<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h2> Register Event Form</h2>
<form action="registerEvent" method="post">
	<table>
		<tr><td>Event Id</td> <td><input type="number" name="eventId"/></td></tr>
		<tr><td>Event Name</td> <td><input type="text" name="eventName"/></td></tr>
		<tr><td>Location</td> <td><input type="text" name="location"/></td></tr>
		<tr><td>Capacity</td> <td><input type="text" name="capacity"/></td></tr>
		<tr><td>Event status</td> <td><input type="text" name="eventStatus"/></td></tr>
		<tr><td><input type="submit" value="Register Event"/></td></tr>
	</table>
</form>
</body>
</html>