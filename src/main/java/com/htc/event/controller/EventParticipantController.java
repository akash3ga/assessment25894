package com.htc.event.controller;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.htc.event.model.Participant;
import com.htc.event.service.EventParticipantService;
import com.htc.event.to.EventResponse;
import com.htc.event.to.EventTO;
import com.htc.event.to.ParticipantTO;

@Controller
public class EventParticipantController {

	@Autowired
	EventParticipantService eventParticipantService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String Welcome() {
		System.out.println("Executed welcomePage() method..");
		return "menu";
	}

	@RequestMapping(value = "/registerEventForm", method = RequestMethod.GET)
	public String registerEventForm() {
		return "event-registration";
	}

	@PostMapping("/registerEvent")
	public String addCustomer(@ModelAttribute @Valid EventTO eventTO, BindingResult bindingResult,
			RedirectAttributes redirectAttributes) {
		System.out.println(eventTO);
		EventResponse response = new EventResponse();
		if (bindingResult.hasErrors()) {
			System.out.println("in error");
			Map<String, String> errors = bindingResult.getFieldErrors().stream()
					.collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));
			response.setValidated(false);
			response.setErrorMessages(errors);
			return "event-registration";
		} else {
			response.setValidated(true);
			boolean result = eventParticipantService.regiterEvent(eventTO);
			if (result) {
				response.setStatusMessage("Event record saved successfully");
				redirectAttributes.addFlashAttribute("eventName", eventTO.getEventName());
				redirectAttributes.addFlashAttribute("msg", "Added successfully");
				return "redirect:/registerEventSuccess";
			} else {
				response.setStatusMessage("Error while saving event record");
				return "event-registration";
			}
		}
	}

	@GetMapping("/registerEventSuccess")
	public String registerEventSuccess() {
		return "register-eventsuccess";
	}

	@RequestMapping(value = "/registerParticipantForm", method = RequestMethod.GET)
	public String registerParticipantForm() {
		return "participant-registration";
	}

	@PostMapping("/registerParticipant")
	public String registerParticipant(@ModelAttribute @Valid ParticipantTO participantTO, BindingResult result,
			RedirectAttributes redirectAttributes) {

		System.out.println(participantTO);

		if (result.hasErrors()) {
			return "participant-registration";
		} else {
			boolean status = eventParticipantService.regiterParticipant(participantTO);
			if (status) {
				redirectAttributes.addFlashAttribute("participantName", participantTO.getParticipantName());
				redirectAttributes.addFlashAttribute("msg", "Added successfully");
				return "redirect:/registerParticipantSuccess";
			} else
				return "participant-registration";
		}

	}
	@GetMapping("/registerParticipantSuccess")
	public String registerParticipantSuccess() {
		return "register-participantsuccess";
	}
	@GetMapping("/searchParticipantDetails")
	public String searchParticipantDetails() {
		return "search-participants";
	}
	
	@PostMapping("/getParticipantDetails")
	public ModelAndView getPolicyDetails(@RequestParam(name="location") String location) {
		ModelAndView mv = new ModelAndView();
		System.out.println("in controller getPolicyDetails"+location);
		List<Participant> participants = eventParticipantService.getParticipantDetails(location);
		if(participants.isEmpty()) {
			mv.setViewName("no-participants");		
			mv.addObject("location", location);
		}
		else {
			mv.setViewName("list-participants");
			mv.addObject("participants", participants);
		}
		return mv;	
	}
}
