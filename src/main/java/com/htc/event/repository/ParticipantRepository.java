package com.htc.event.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.htc.event.model.Participant;

@Repository
public interface ParticipantRepository extends JpaRepository<Participant, Integer> {

}
