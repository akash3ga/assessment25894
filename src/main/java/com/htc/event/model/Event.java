package com.htc.event.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Table(name="events")
public class Event {
	@Id
	@Column(name="eventid")
	private int eventId;
	@Column(name="eventname")
	private String eventName;
	private String location;
	private int capacity;
	@Column(name="eventstatus")
	private @NotEmpty(message = "Event status is Mandatory") @Size(min = 1, max = 1, message = "Event Status Should be 'A' or 'I'") @Pattern(regexp = "A", message = "Event Status Should be 'A' or 'I'") String eventStatus;
	
	@OneToMany(mappedBy="event", cascade=CascadeType.ALL)
	Set<Participant> participants;
	
	public Event() {}

	public Event(int eventId, String eventName, String location, int capacity, String eventStatus) {
		super();
		this.eventId = eventId;
		this.eventName = eventName;
		this.location = location;
		this.capacity = capacity;
		this.eventStatus = eventStatus;
	}

	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public String getEventStatus() {
		return eventStatus;
	}

	public void setEventStatus(@NotEmpty(message = "Event status is Mandatory") @Size(min = 1, max = 1, message = "Event Status Should be 'A' or 'I'") @Pattern(regexp = "A", message = "Event Status Should be 'A' or 'I'") String string) {
		this.eventStatus = string;
	}

	public Set<Participant> getParticipants() {
		return participants;
	}

	public void setParticipants(Set<Participant> participants) {
		this.participants = participants;
	}

	@Override
	public String toString() {
		return "Event [eventId=" + eventId + ", eventName=" + eventName + ", location=" + location + ", capacity="
				+ capacity + ", eventStatus=" + eventStatus + "]";
	}

}
