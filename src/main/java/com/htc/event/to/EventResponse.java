package com.htc.event.to;

import java.util.Map;

public class EventResponse {
	
	private boolean validated;
	private Map<String, String> errorMessages;
	EventTO EventTO;
	String statusMessage;
	
	public boolean isValidated() {
		return validated;
	}
	public void setValidated(boolean validated) {
		this.validated = validated;
	}
	public Map<String, String> getErrorMessages() {
		return errorMessages;
	}
	public void setErrorMessages(Map<String, String> errorMessages) {
		this.errorMessages = errorMessages;
	}
	public EventTO getEventTO() {
		return EventTO;
	}
	public void setEventTO(EventTO eventTO) {
		EventTO = eventTO;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	@Override
	public String toString() {
		return "EventResponse [validated=" + validated + ", errorMessages=" + errorMessages + ", EventTO=" + EventTO
				+ ", statusMessage=" + statusMessage + "]";
	}	
}
