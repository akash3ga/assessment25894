package com.htc.event.to;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class ParticipantTO {
	
	@NotNull(message="Participant ID is Mandatory")
	private int participantId;
	@NotEmpty(message="Participant Name is Mandatory")
	private String participantName;
	@NotEmpty(message="Mobile number is Mandatory")
	private String mobileNo;
	@NotEmpty(message="Address is Mandatory")
	private String address;
	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	@NotNull(message="Event Id is Mandatory")
	private int eventId;
	
	public ParticipantTO() {}

	public ParticipantTO(@NotEmpty(message = "Participant ID is Mandatory") int participantId,
			@NotEmpty(message = "Participant Name is Mandatory") String participantName,
			@NotEmpty(message = "Mobile number is Mandatory") String mobileNo,
			@NotEmpty(message = "Address is Mandatory") String address,
			@NotEmpty(message = "Event Id is Mandatory") int eventId) {
		super();
		this.participantId = participantId;
		this.participantName = participantName;
		this.mobileNo = mobileNo;
		this.address = address;
		this.eventId = eventId;
	}


	public int getParticipantId() {
		return participantId;
	}

	public void setParticipantId(int participantId) {
		this.participantId = participantId;
	}

	public String getParticipantName() {
		return participantName;
	}

	public void setParticipantName(String participantName) {
		this.participantName = participantName;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "ParticipantTO [participantId=" + participantId + ", participantName=" + participantName + ", mobileNo="
				+ mobileNo + ", address=" + address + "]";
	}
	
	
}
