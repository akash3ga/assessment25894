package com.htc.event.to;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class EventTO implements Serializable{
	@NotNull(message="Event Id is Mandatory")
	private int eventId;
	@NotEmpty(message="Event name is Mandatory")
	private String eventName;
	@NotEmpty(message="location is Mandatory")
	private String location;
	@NotNull(message="capacity is Mandatory")
	private int capacity;
	@NotEmpty(message="Event status is Mandatory")
	@Size(min = 1, max = 1, message = "Event Status Should be 'A' or 'I'")
	@Pattern(regexp = "[AI]" , message = "Event Status Should be 'A' or 'I'")
	private String eventStatus;
	
	public EventTO() {}

	public EventTO(@NotEmpty(message = "Event Id is Mandatory") int eventId,
			@NotEmpty(message = "Event name is Mandatory") String eventName,
			@NotEmpty(message = "location is Mandatory") String location,
			@NotEmpty(message = "capacity is Mandatory") int capacity,
			@NotEmpty(message = "Event status is Mandatory") @NotEmpty(message = "Event status is Mandatory") @Size(min = 1, max = 1, message = "Event Status Should be 'A' or 'I'") @Pattern(regexp = "A", message = "Event Status Should be 'A' or 'I'") String eventStatus) {
		super();
		this.eventId = eventId;
		this.eventName = eventName;
		this.location = location;
		this.capacity = capacity;
		this.eventStatus = eventStatus;
	}

	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public @NotEmpty(message = "Event status is Mandatory") @Size(min = 1, max = 1, message = "Event Status Should be 'A' or 'I'") @Pattern(regexp = "A", message = "Event Status Should be 'A' or 'I'") String getEventStatus() {
		return eventStatus;
	}

	public void setEventStatus(@NotEmpty(message = "Event status is Mandatory") @Size(min = 1, max = 1, message = "Event Status Should be 'A' or 'I'") @Pattern(regexp = "A", message = "Event Status Should be 'A' or 'I'") String eventStatus) {
		this.eventStatus = eventStatus;
	}

	@Override
	public String toString() {
		return "EventTO [eventId=" + eventId + ", eventName=" + eventName + ", location=" + location + ", capacity="
				+ capacity + ", eventStatus=" + eventStatus + "]";
	}




}
