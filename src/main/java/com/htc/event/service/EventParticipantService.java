package com.htc.event.service;

import java.util.List;

import com.htc.event.model.Participant;
import com.htc.event.to.EventTO;
import com.htc.event.to.ParticipantTO;

public interface EventParticipantService {
	public boolean regiterEvent(EventTO eventTO);
	public boolean regiterParticipant(ParticipantTO participantTO);
	public List<Participant> getParticipantDetails(String location);
}
