package com.htc.event.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Service;

import com.htc.event.model.Event;
import com.htc.event.model.Participant;
import com.htc.event.repository.EventRepository;
import com.htc.event.repository.ParticipantRepository;
import com.htc.event.to.EventTO;
import com.htc.event.to.ParticipantTO;

@Service
public class EventParticipantServiceImple implements EventParticipantService {

	@Autowired
	EventRepository eventRepository;

	@Autowired
	ParticipantRepository participantRepository;

	@Autowired
	JdbcTemplate jdbcTemplate;

	private EventTO getEventTO(Event event) {
		EventTO eventTO = new EventTO();
		eventTO.setEventId(event.getEventId());
		eventTO.setEventName(event.getEventName());
		eventTO.setLocation(event.getLocation());
		eventTO.setEventStatus(event.getEventStatus());
		eventTO.setCapacity(event.getCapacity());
		return eventTO;
	}

	private Event getEvent(EventTO eventTO) {
		Event event = new Event();
		event.setEventId(eventTO.getEventId());
		event.setEventName(eventTO.getEventName());
		event.setLocation(eventTO.getLocation());
		event.setEventStatus(eventTO.getEventStatus());
		event.setCapacity(eventTO.getCapacity());
		return event;
	}

	private Participant getParticipant(ParticipantTO participantTO) {
		Participant participant = new Participant();
		participant.setParticipantId(participantTO.getParticipantId());
		participant.setParticipantName(participantTO.getParticipantName());
		participant.setMobileNo(participantTO.getMobileNo());
		participant.setAddress(participantTO.getAddress());
		return participant;
	}

	private ParticipantTO getParticipantTO(Participant participant) {
		ParticipantTO participantTO = new ParticipantTO();
		participantTO.setParticipantId(participant.getParticipantId());
		participantTO.setParticipantName(participant.getParticipantName());
		participantTO.setMobileNo(participant.getMobileNo());
		participantTO.setAddress(participant.getAddress());
		participantTO.setParticipantId(participant.getParticipantId());
		return participantTO;
	}

	@Override
	public boolean regiterEvent(EventTO eventTO) {
		System.out.println("in service imple event register");
		if (eventRepository.save(getEvent(eventTO)) != null)
			return true;
		else
			return false;
	}

	@Override
	public boolean regiterParticipant(ParticipantTO participantTO) {
		System.out.println("in service imple participant register");
		Event event = new Event();
		event.setEventId(participantTO.getEventId());
		Participant participant = getParticipant(participantTO);
		participant.setEvent(event);
		if (participantRepository.save(participant)!= null)
			return true;
		else
			return false;

	}

	@Override
	public List<Participant> getParticipantDetails(String location) {
		System.out.println("in service imple getParticipantDetails");
		String query = "SELECT p.participantid,p.participantname, p.mobileno, p.address,  e.eventid, e.eventname, e.location, e.capacity, e.eventstatus\r\n"
				+ "FROM events e\r\n"
				+ "INNER JOIN participants p\r\n"
				+ "ON (e.eventid=p.eventid)\r\n"
				+ "WHERE e.location=?";
		List<Participant> participants = jdbcTemplate.query(query, new Object[] { location },
				new ResultSetExtractor<List<Participant>>() {

					@Override
					public List<Participant> extractData(ResultSet rs) throws SQLException, DataAccessException {
						ArrayList<Participant> participants = new ArrayList<Participant>();
						while (rs.next()) {
							Participant participant = new Participant();
							participant.setParticipantId(rs.getInt(1));
							participant.setParticipantName(rs.getString(2));
							participant.setMobileNo(rs.getString(3));
							participant.setAddress(rs.getString(4));
							Event event = new Event();
							event.setEventId(rs.getInt(5));
							event.setEventName(rs.getString(6));
							event.setLocation(rs.getString(7));
							event.setCapacity(rs.getInt(8));
							event.setEventStatus(rs.getString(9));
							participant.setEvent(event);
							System.out.println(participant);
							participants.add(participant);
						}
						return participants;
					}
				});
		return participants;
	}
}
